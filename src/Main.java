import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {
	public static int num_plates_x = 2;
	public static int num_plates_y = 2;
	public static int num_sensors_per_plate = 4;
	public static Plate[][] plates;
	public static int frequncy = 1500;

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Force detection");
		SplitPane split_pane = new SplitPane();
		split_pane.setPrefSize(1280, 720);
		VBox menu = new VBox(20);
		ProgressIndicator pi = new ProgressIndicator(0);
		pi.getStyleClass().add("progress");
		pi.setPrefSize(100, 100);
		pi.setVisible(false);
		Button calibrate = new Button("Calibrate sensors");
		calibrate.getStyleClass().add("calibrate");
		calibrate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				pi.setVisible(true);
				pi.setProgress(0);
				calibrate.setDisable(true);
				Timeline fiveSecondsCalibrate = new Timeline(
						new KeyFrame(Duration.millis(500), new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								pi.setProgress(pi.getProgress() + 0.1);
							}
						}));
				fiveSecondsCalibrate.setCycleCount(10);
				fiveSecondsCalibrate.play();
				fiveSecondsCalibrate.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("Callibration ended");
						calibrate.setDisable(false);
						pi.setVisible(false);
					}
				});
			}
		});

		menu.getChildren().addAll(calibrate, pi);
		GridPane gridPane = new GridPane();
		gridPane.setMinSize(400, 400);
		gridPane.setPrefSize(600, 600);
		gridPane.setPadding(new Insets(25, 25, 25, 25));
		gridPane.setVgap(num_plates_x);
		gridPane.setHgap(num_plates_y);
		plates = new Plate[num_plates_x][num_plates_y];
		gridPane.setAlignment(Pos.CENTER);

		for (int i = 0; i < num_plates_x; i++) {
			for (int j = 0; j < num_plates_y; j++) {
				Plate p = new Plate(num_sensors_per_plate);
				p.getStyleClass().add("plate");
				plates[i][j] = p;
				gridPane.add(p, i, j);
			}
		}
		split_pane.getItems().addAll(gridPane, menu);
		Scene grid_scene = new Scene(split_pane);
		grid_scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		primaryStage.setScene(grid_scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		Timeline fiveSecondsWonder = new Timeline(
				new KeyFrame(Duration.millis(frequncy), new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						for (int i = 0; i < plates.length; i++) {
							for (int j = 0; j < plates[i].length; j++) {
								plates[i][j].update();
							}
						}
					}
				}));
		fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
		fiveSecondsWonder.play();
		launch(args);
	}
}
